
var md5 = require("utils/md5.js");

var member = wx.getStorageSync('member');

App({
 
  showModel:function(){
     wx.showToast({
      title: '正在加载....',
      icon: 'loading',
      duration: 5000
    });
  },

  globalData: {
    data: {},
    // 通用查询接口
    http_api: "http://www.ps.com/index.php?c=api&m=data2&auth=" + md5.hex_md5("2A68E3A172E9EF1ED199"),
    // 移动端接口
    mobile_api: "http://www.ps.com/index.php?c=api&m=data2&auth=" + md5.hex_md5("NGE3NJAWYWQXOGRHYTG3MZBJZJDHZDDJZDC5MWQZZJNKYTHMMTJLZJRIMZRIZJI0"),
    // 登录认证接口
    member_api: "http://www.ps.com/index.php?auth_code=" + md5.hex_md5("NGE3NJAWYWQXOGRHYTG3MZBJZJDHZDDJZDC5MWQZZJNKYTHMMTJLZJRIMZRIZJI0") + "&auth_uid=" + member.uid + "&auth_password=" + md5.hex_md5(member.username + member.salt),
  }
})