<?php


// 此文件放到cms目录下：\diy\dayrui\helpers\my_helper.php
// 如果你cms已经定义过自定义函数的话，直接把下面三个函数放在原文件的下面即可

// 格式化查询列表list
function dr_my_list($data) {

    if ($data['return']) {
        $rt = array();
        foreach ($data['return'] as $t) {
            if (is_numeric($t['inputtime'])) {
                $t['inputtime'] = dr_date( $t['inputtime']);
            }
            if (is_numeric($t['uid'])) {
                $t['avatar'] = dr_avatar( $t['uid']);
            }
            $t['thumb'] = dr_url_prefix(dr_thumb( $t['thumb']));
            $rt[] = $t;
        }
        $data['return'] = $rt;
    }

    return $data;
}

// 我的收藏
function dr_my_favorite($mid, $uid, $pagesize, $page = 1) {

    $mid = dr_safe_replace($mid);
    $uid = intval($uid);
    $page = max(1, intval($page));

    $e = intval($pagesize);
    $s = ($page-1) * $e;


    $ci	= &get_instance();
    $prefix = $ci->db->dbprefix(SITE_ID.'_'.$mid);
    $sql = "select * from {$prefix} where id in(select  cid from `{$prefix}_favorite` where  uid={$uid} order by  inputtime) Limit {$s},{$e}";
    $data = $ci->db->query($sql)->result_array();

    $now = array();
    foreach ($data as $t) {
        $t['thumb'] = dr_url_prefix(dr_thumb( $t['thumb']));
        $t['inputtime'] = dr_date( $t['inputtime']);
        $now[] = $t;
    }
    return $now;
}

// 我的评论 
function dr_my_comment($mid, $uid, $pagesize, $page = 1) {

    $mid = dr_safe_replace($mid);
    $uid = intval($uid);
    $page = max(1, intval($page));

    $e = intval($pagesize);
    $s = ($page-1) * $e;


    $ci	= &get_instance();
    $prefix = $ci->db->dbprefix(SITE_ID.'_'.$mid);
    $sql = "select a.* from `{$prefix}_comment_data_0` as a where a.uid={$uid} order by a.inputtime Limit {$s},{$e}";
    $data = $ci->db->query($sql)->result_array();

    $now = array();
    foreach ($data as $t) {
        $t['inputtime'] = dr_date( $t['inputtime']);
        $now[] = $t;
    }
    return $now;
}